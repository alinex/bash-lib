#!/bin/bash

# Build combined bash-lib
#
# https://gitlab.com/alinex/bash-lib/blob/master/README.md

source_dir=$(dirname $(readlink -f "${BASH_SOURCE[0]:-$(pwd)/x}"))

# Configuration
declare -A lib
lib[base]="core, colors, log"
lib[process]="core, colors, log, process"
lib[info]="core, colors, log, info"
lib[psql]="core, colors, log, psql"
lib[all]="core, colors, log, process, info, psql"

VERSION=$(git describe --abbrev=0 --tags 2>/dev/null); VERSION=${VERSION:-master}
REVISION=$(git rev-parse --verify master)
GITLAB=$(git config --get remote.origin.url | sed 's/\.git$//')

# Load libraries
source "$source_dir/../src/include/log.bash"

# Parameter checking
parsed=$(getopt --options=:Vh --longoptions=version,help --name "$0" -- "$@")
[ $? -eq 0 ] || log_exit ALERT "Incorrect options provided!"
eval set -- "$parsed"
while true; do
    case "$1" in
        -V|--version) echo "$(basename $0) from bash-lib version $VERSION ($REVISION)"; exit 0 ;;
        -h|--help) source "$(path)/$(basename $0).help"; exit 0 ;;
        --) shift; break ;;
        *) break ;;
    esac
done
[ $# -gt 0 ] && log WARN "No parameters are supported, only -h to show help - ignoring them"

# check if tools are installed or install them
#log DEBUG "Check for needed 3rd party tools"
#command -v perltidy >/dev/null
#if [ $? -ne 0 ]; then
#    log WARN "Command perltidy will be installed"
#    log_cmd sudo apt-get install perltidy -y >/dev/null \
#    || log_exit ALERT "Could not install perltidy"
#fi

# check that everything is up to date
log DEBUG "Inform if not everything is checked in"
cd "$(path)/.."
(git diff --exit-code && git diff --cached --exit-code) >/dev/null 2>/dev/null
[ $? -eq 0 ] || \
    log WARN "Not everything is checked in to the local git repository!"
[ "$REVISION" = $(git rev-parse --verify origin/master) ] || \
    log WARN "Local changes are not pushed to master!"

info_comment="# Bash helper library and tools by A.Schilling:\n# $GITLAB/blob/$VERSION/README.md\n"
sed_gitlab=$(echo "$GITLAB" | sed -e 's/[\/&]/\\&/g')

log HEADING "Building libraries..."
rm -rf dist 2>/dev/null
mkdir -p dist |& log || log_exit ALERT "Could not create distribution directory"
for pack in "${!lib[@]}"; do
    parts="${lib[$pack]}"
    dest="dist/$pack.bash"
    log INFO "Generation dist/$pack.bash including $parts"
    # Header
    echo "#!/bin/bash" > $dest
    echo -e "$info_comment" >> $dest
    echo "# Automatically combined bash-lib including: $parts" >> $dest
    echo >> $dest
    # Concatenate libs
    parts=$(echo "$parts" | sed 's/ //g')
    bash -c "egrep -hv 'source \"\\\$source_dir/|source_dir=' \
        src/include/{$parts}.bash \
        | sed -e '/^\s*#/d;s/\s\s*#\s.*//g;s/^[[:space:]]*//;/^$/d' \
        | sed 's/^VERSION=.*$/VERSION=$VERSION/g' \
        | sed 's/^REVISION=.*$/REVISION=$REVISION/g' \
        | sed 's/^GITLAB=.*$/GITLAB=$sed_gitlab/g' \
        | perl -0777pe 's/\\\\\n//sg'
        " >> $dest
done

log HEADING "Building commands..."
for file in src/bin/*; do
    dest="dist/$(basename $file)"
    case $(head -n 1 $file) in
        \#!/bin/bash)
            used_lib=$(grep "# DIST include" $file | awk '{ print $4 }')
            if [ -n "$used_lib" ]; then
                [ -e "dist/$used_lib" ] \
                || log_exit ALERT "Library $used_lib not existing, referenced in $file"
                log INFO "Building $dest using $used_lib library"
            else
                log INFO "Building $dest"
            fi
            # Header
            echo "#!/bin/bash" > $dest
            echo -e "$info_comment" >> $dest
            if [ -n "$used_lib" ]; then
                # script till library inclusion
                sed -e '/# DIST include/,$d' $file \
                | sed "s/^VERSION=.*$/VERSION=$VERSION/g" \
                | sed "s/^REVISION=.*$/REVISION=$REVISION/g" \
                | sed "s/^GITLAB=.*$/GITLAB=$sed_gitlab/g" \
                | sed -e '/^\s*#/d;s/\s\s*#\s.*//g;s/^[[:space:]]*//;/^$/d' \
                >> $dest
                # add library
                egrep -v '^#' "dist/$used_lib" | sed '/^$/d' >> $dest
                # Going on in script
                sed -e '1,/# DIST include/d' $file | tail -n +2 \
                | sed "s/^VERSION=.*$/VERSION=$VERSION/g" \
                | sed "s/^REVISION=.*$/REVISION=$REVISION/g" \
                | sed "s/^GITLAB=.*$/GITLAB=$sed_gitlab/g" \
                | sed -e '/^\s*#/d;s/\s\s*#\s.*//g;s/^[[:space:]]*//;/^$/d' \
                >> $dest
            else
                cat $file \
                | sed "s/^VERSION=.*$/VERSION=$VERSION/g" \
                | sed "s/^REVISION=.*$/REVISION=$REVISION/g" \
                | sed "s/^GITLAB=.*$/GITLAB=$sed_gitlab/g" \
                | sed -e '/^\s*#/d;s/\s\s*#\s.*//g;s/^[[:space:]]*//;/^$/d' \
                >> $dest
            fi
            ;;
        \#!/usr/bin/perl*)
            log INFO "Compress $dest"
            cp $file $dest
            #perltidy -i=0 -act=2 -pt=2 -ci=0 -tso -nsfs -dsm -dws \
            #    -nwls="= + - / *" -nwrs="= + - / *" -tqw -dnl -kbl=0 \
            #    --delete-all-comments "$file" -o "$dest" |& log
            sed -i "2i$info_comment" "$dest"
            sed -i "s/###VERSION###/$VERSION/g" "$dest"
            sed -i "s/###REVISION###/$REVISION/g" "$dest"
            sed -i "s/###GITLAB###/$sed_gitlab/g" "$dest"
            ;;
        *)
            [[ "$file" = *.md ]] && continue
            log INFO "Copying to $dest"
            cp "$file" dist |& log
            ;;
    esac
    chmod a+rx "$dest" |& log
done

log HEADING "Add meta data..."

cp -v README.md LICENSE.md docs/CHANGELOG.md dist/ |& log AUTO_INFO

log HEADING "DONE."
